import React from 'react'

const User = (props) => 
  (<ul className="user">
    <li>
      <p>User name: <strong>{props.userName}</strong></p>
      <p>Age: <strong>{props.userAge}</strong></p>
      <button onClick={props.onDeleteUser}>Delete this user</button>
    </li>
  </ul>
  );

export default User