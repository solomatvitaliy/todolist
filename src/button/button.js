import React from 'react'

const Button = (props) => (
  <div> 
    <button className="button" onClick={props.onChangeTitle}>Change Title</button>
  </div>
);

export default Button