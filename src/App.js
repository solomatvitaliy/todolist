import React, { Component } from 'react';
import './App.css';
import Button from './button/button';
import User from './user/user'

class Todo {
  constructor(title) {
    this.title = title;
  }
}

class App extends Component {

  state = {
    randomStr: 'Random String',
    title: 'ToDo List',
    users: [
      {name: 'Vasya', age: 24},
      {name: 'Petya', age: 20},
      {name: 'Vanya', age: 23},
      {name: 'Viktor', age: 25}
    ],
    showUsers: true,
    todos: [],
    todoInput: ''
  }

  handleCreateTodo = () => {
    this.setState({
      todos: [...this.state.todos, new Todo(this.state.todoInput)],
      todoInput: ''
    })
  }

  handleTodoInputChange = (e) => {
    this.setState({ todoInput: e.target.value })
  }

  handleDeleteTodo = (index) => {
    let todos = this.state.todos.concat()
    todos.splice(index, 1)
    this.setState({todos})
  }

  onKeyPress = event => {
    if (event.key === 'Enter') {
      this.setState({
        todos: [...this.state.todos, new Todo(this.state.todoInput)],
        todoInput: ''
      })
    }
  }

  handleSubmit = (event) => {
    let name = event.target.value;
    let age = 25;
    let users = this.state.users.push(name, age)
    this.setState({users})
  }

  changeTitleHandler = () => {
    const oldTitle = this.state.title;
    const newTitle = 'Changed Title';
    this.setState({
      title: newTitle
    })
  }

  toggleUsersList = () => {
    this.setState({
      showUsers: !this.state.showUsers
    })
  }

  handleInput = (event) => {
    this.setState({
      randomStr: event.target.value
    })
  }

  deleteUser(index) {
    let users = this.state.users.concat()
    users.splice(index, 1)
    this.setState({users})
  }

  render() {

    const users = this.state.users;

    return (
      <div className="App">

        <header className="App-header">
          <div className="header__container">

            <h1 className="header__title">
              {this.state.title}
            </h1>

            <Button onChangeTitle={this.changeTitleHandler}/>

            <h3>
              {this.state.randomStr}
            </h3>

            <fieldset className="fieldset">
              <label>
                Enter your random string
                <input className='input' type="text" name="titleHandler" onChange={this.handleInput}>
              </input>
              </label>
            </fieldset>

            {/* <fieldset className="fieldset">
              <legend>Your personal info</legend>
              <label>
                Enter your username
                <input type="text" name="username" className='input'>
              </input>
              </label>
              <label>
                Enter your age
                <input type="number" name="userage" className='input'>
              </input>
              </label>
              <input type="submit" value="Submit"></input>
            </fieldset> */}

            <div className="todoList">
              <h3>Input todo</h3>
              <input 
                value={this.state.todoInput}  
                onChange={this.handleTodoInputChange} 
                placeholder="Input todo and press enter"
                onKeyPress={this.onKeyPress}
              />
              <button className="button" onClick={this.handleCreateTodo}>Create</button>
              <ul>
                {this.state.todos.map((todo, index) => <li>{todo.title}
                <button 
                  key={index} className="button" onClick={this.handleDeleteTodo.bind(this, index)}>
                  Delete
                </button>
                </li>)}
              </ul>
            </div>
            
            <div> 
              <button className="button" onClick={this.toggleUsersList}>
                Hide Users List
              </button>
            </div>

            { this.state.showUsers 
              ? this.state.users.map((user, index) => {
                return (
                  <User
                    key={index}
                    userName={user.name}
                    userAge={user.age}
                    onDeleteUser={this.deleteUser.bind(this, index)}
                  />
                ) 
              })
              : null
            }

            {/* <User userName={'Lena'} userAge={'22'}>
              <p>Test</p>
            </User> */}

            {/* <User userName={users[0].name} userAge={users[0].age} />
            <User userName={users[1].name} userAge={users[1].age} />
            <User userName={users[2].name} userAge={users[2].age} /> */}

          </div>
        </header>
      </div>
    );
  }
}

export default App;
